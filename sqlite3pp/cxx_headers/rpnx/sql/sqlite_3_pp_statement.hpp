//
// Created by Ryan P. Nicholl on 6/25/22.
//

#ifndef FSSNAPSH_SQLITE_3_PP_STATEMENT_HPP
#define FSSNAPSH_SQLITE_3_PP_STATEMENT_HPP

#include <sqlite3.h>
#include <string>
#include <atomic>
namespace rpnx
{
    namespace sqlite3pp
    {
        class database;
        class statement
        {
        private:


            static std::atomic<std::size_t> total;
            ::sqlite3_stmt * m_st;

            template <typename T>
            int bind_helper2(std::size_t index, T && t)
            {
                bind_at(index+1, t);
                return 1;
            }

            template <typename Tuple, std::size_t ... indexes>
            void bind_helper1(Tuple tpl, std::integer_sequence<std::size_t, indexes...>)
            {
                using intarray= int[];
                (void) intarray{ 0, bind_helper2(indexes, std::get<indexes>(tpl))...};
            }

            template <typename T>
            int results_helper2(std::size_t index, T && t)
            {
                result_at(index, t);
                return 1;
            }

            template <typename Tuple, std::size_t ... indexes>
            void results_helper1(Tuple &tpl, std::integer_sequence<std::size_t, indexes...>)
            {
                using intarray= int[];
                (void) intarray{ 0, results_helper2(indexes, std::get<indexes>(tpl))...};
            }
        public:
            statement(::sqlite3 * db, std::string text);
            statement(database & db, std::string text);
            explicit statement(::sqlite3_stmt * stment);
            ~statement();

            statement(statement const &) = delete;
            statement(statement &&) noexcept ;

            statement & operator=(statement const &) = delete;
            statement & operator=(statement &&) = delete;

            void bind_at(std::size_t index, std::int64_t i);
            void bind_at(std::size_t index, std::string const & str);

            void result_at(std::size_t index, std::string & output);
            void result_at(std::size_t index, std::int64_t & output);


            template <typename ...Ts>
            void get_results(Ts && ... ts)
            {
                auto tuplex = std::forward_as_tuple(std::forward<Ts>(ts)...);
                auto constexpr seq = std::make_index_sequence<std::tuple_size<std::tuple<Ts...>>::value>();
                results_helper1(tuplex, seq);
            }

            template <typename ... Ts>
            void get_results_as_tuple(std::tuple<Ts...> & tuple)
            {
                results_helper1(tuple, std::make_index_sequence<std::tuple_size<std::tuple<Ts...>>::value>());
            }



            template <typename ...Ts>
            void bind(Ts && ... ts)
            {
                auto tuplex = std::forward_as_tuple(std::forward<Ts>(ts)...);
                auto constexpr seq = std::make_index_sequence<std::tuple_size<std::tuple<Ts...>>::value>();
                bind_helper1(tuplex, seq);
            }





            void reset();

            ::sqlite3_stmt * underlying();

            int step();

            std::string m_text;
        };



    } // rpnx
} // sqlite3



#endif //FSSNAPSH_SQLITE_3_PP_STATEMENT_HPP
