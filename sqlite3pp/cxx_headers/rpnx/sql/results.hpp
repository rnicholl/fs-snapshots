//
// Created by Ryan P. Nicholl on 6/25/22.
//

#ifndef FSSNAPSH_RESULTS_HPP
#define FSSNAPSH_RESULTS_HPP

#include "sqlite_3_pp_statement.hpp"

namespace rpnx
{
    namespace sqlite3pp
    {
        template <typename ... Ts>
        class results
        {
            statement & m_st;
        public:
            explicit results(statement & st);

            operator std::tuple<Ts ...>()
            {

            }
        };

        template<typename... Ts>
        results<Ts...>::results(statement &st)
        :m_st(st)
        {

        }

    } // rpnx
} // sqlite3pp

#endif //FSSNAPSH_RESULTS_HPP
