//
// Created by Ryan P. Nicholl on 6/25/22.
//

#ifndef FSSNAPSH_DATABASE_HPP
#define FSSNAPSH_DATABASE_HPP
#include <filesystem>
#include <sqlite3.h>
#include <iostream>
#include <iterator>

#include "sqlite_3_pp_statement.hpp"

namespace rpnx
{
    namespace sqlite3pp
    {

        class database
        {
            sqlite3 * m_db;
        public:
            explicit database(std::filesystem::path path);
            ~database();

            statement create_statement(std::string const & str);

            sqlite3 *underlying();

            template <typename ... Ts>
            void exec(/* language=SQLite */std::string const &/* language=SQLite */ str, Ts && ... ts);

            template < typename It, typename ... Ts >
            void query( It output , std::string const & str,  Ts && ... ts );

        };

        template<typename... Ts>
        void database::exec(const  std::string &str /* language=SQLite */, Ts && ... ts)
        {
            statement st{*this, str};
            st.bind(ts...);
            st.step();
        }

        template< typename It, typename... Ts >
        void database::query(It output, const std::string &str,    Ts &&... ts )
        {
            using result_type = typename std::iterator_traits<It>::value_type;
            statement st{*this, str};
            st.bind(std::forward<Ts>(ts)...);
            int result;
            do
            {
                result = st.step();
                if (result == SQLITE_DONE) break;
                if (result != SQLITE_ROW)
                {
                    throw std::runtime_error("Error");
                }
                if constexpr (std::is_same_v<void, result_type>)
                {
                    using ctype = typename decltype(output)::container_type::value_type;
                    ctype res;
                    st.get_results_as_tuple(res);
                    *output++ = res;
                }
                else if constexpr (!std::is_same_v<decltype(*output), result_type> )
                {
                    result_type res;
                    st.get_results_as_tuple(res);
                    *output++ = res;
                }
                else
                {
                    st.get_results_as_tuple(*output++);
                }
            } while (result == SQLITE_ROW);
        }

    } // rpnx
} // sqlite3pp

#endif //FSSNAPSH_DATABASE_HPP
