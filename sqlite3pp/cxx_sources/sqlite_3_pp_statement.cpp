//
// Created by Ryan P. Nicholl on 6/25/22.
//

#include "rpnx/sql/sqlite_3_pp_statement.hpp"
#include "rpnx/sql/database.hpp"
#include <iostream>

namespace rpnx
{
    namespace sqlite3pp
    {
        std::atomic<std::size_t> statement::total {};
        statement::statement(::sqlite3 *db, std::string text)
        {
#ifndef NDEBUG
            std::cerr << "Create Statement " << this << " " << text << " total existing: " << ++total << std::endl;
#endif
            m_text = text;
            auto result = sqlite3_prepare_v2(db, text.c_str(), -1, &m_st, nullptr);

            if (result != SQLITE_OK) throw std::runtime_error( "Failed to allocate SQL statement: " + std::string(sqlite3_errmsg(db)));
        }

        statement::statement(database & db, std::string text)
        : statement(db.underlying(), std::move(text))
        {
        }


        statement::~statement()
        {
            #ifndef NDEBUG
                std::cerr << "Destroy statement " << this << " " << m_text << " valid=" << (m_st != nullptr) << " total existing: " << --total  << std::endl;
            #endif
            if (m_st != nullptr)
            {
                sqlite3_finalize(m_st);
            }
        }

        void statement::reset()
        {
            ::sqlite3_reset(m_st);
            // TODO: Errors
        }

        ::sqlite3_stmt *statement::underlying()
        {
            return m_st;
        }

        int statement::step()
        {
            std::cerr << "exec statement" << std::endl;
            auto result = sqlite3_step(m_st);
            return result;
        }

        statement::statement(statement && other) noexcept
        {
           // if (this == &other) return;
           #ifndef NDEBUG
                std::cerr << "Create statement " << this << " from move " << other.m_text << " total exist: " << ++total << std::endl;
            #endif
            m_st = nullptr;

            std::swap(other.m_st, m_st);
            m_text = other.m_text;

        }

        statement::statement(::sqlite3_stmt *stment)
        {
            #ifndef NDEBUG
                std::cerr << "Create statement " << this << " from take over netive statement. " << " total: " << ++total << std::endl;
            #endif
            m_text = "IMPORTED";
            m_st = stment;
        }



        void statement::bind_at(std::size_t index, std::int64_t i)
        {
            auto result = sqlite3_bind_int64(m_st, index, i);
            if (result != SQLITE_OK)
            {
                throw std::runtime_error("Failed to bind parameter");
            }
        }

        void statement::bind_at(std::size_t index, std::string const & str)
        {
            auto result = sqlite3_bind_text(m_st, index, str.c_str(), str.size(), SQLITE_TRANSIENT);
            if (result != SQLITE_OK)
            {
                throw std::runtime_error("Failed to bind parameter");
            }
        }




        void statement::result_at(std::size_t index, std::string &output)
        {
            if (index >= sqlite3_column_count(m_st))
            {
                throw std::out_of_range("result out of range");
            }
            const char * str = (const char *) sqlite3_column_text(m_st, index);
            output = str;
        }

        void statement::result_at(std::size_t index, int64_t &output)
        {
            output = sqlite3_column_int64(m_st, index);
        }


    } // rpnx
} // sqlite3