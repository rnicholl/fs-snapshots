//
// Created by Ryan P. Nicholl on 6/25/22.
//

#include "rpnx/sql/database.hpp"

namespace rpnx
{
    namespace sqlite3pp
    {
        database::database(std::filesystem::path path)
        {
            auto result = sqlite3_open(path.c_str(), &m_db);
            if (result != SQLITE_OK)
            {
                throw std::runtime_error("failed to open database");
            }
        }

        database::~database()
        {
            sqlite3_close(m_db);
        }

        statement database::create_statement(const std::string &str)
        {
            return statement(m_db, str);
        }

        sqlite3 *database::underlying()
        {
            return m_db;
        }




    } // rpnx
} // sqlite3pp