cmake_minimum_required(VERSION 3.18)
project(fssnapshutils)
add_library(fssnapshutils cxx_sources/fssnapsh_db_impl.cpp cxx_sources/snapsh_vault.cpp cxx_headers/fssnapsh/snapsh_vault.hpp cxx_headers/fssnapsh/n546_hash.hpp cxx_headers/fssnapsh/algorithms.hpp cxx_headers/fssnapsh/math.hpp)

target_sources(fssnapshutils PRIVATE
        cxx_headers/fssnapsh/fssnapsh_db.hpp
        cxx_sources/fssnapsh_db.cpp
        )
target_compile_definitions(fssnapshutils PRIVATE FSSNAPSH_PRIVATE)
target_include_directories(fssnapshutils PUBLIC cxx_headers)
target_include_directories(fssnapshutils PRIVATE cxx_headers_private)
target_link_libraries(fssnapshutils PRIVATE SQLite::SQLite3 rpnx-sqlite3pp Rubidium::rubidium)
