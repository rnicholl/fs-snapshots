//
// Created by Ryan P. Nicholl on 7/4/22.
//

#ifndef FSSNAPSH_MATH_HPP
#define FSSNAPSH_MATH_HPP
#include <bit>

namespace rpnx
{
    template <typename I>
    auto constexpr n546_parent(I input) -> I
    {
        auto order = std::countr_zero(input);
        if (input & (I(1) << (order+1)))
        {
            return input ^ (I(1) << (order));
        }
        else
        {
            return input ^ (I(3) << (order));
        }
    }
    template <typename I>
    auto constexpr n546_left_child(I input) -> I
    {
        auto order = std::countr_zero(input);

        // Note: It's not valid to take the child of leaf indexes
        if (order == 0) throw std::invalid_argument("can't take left child of leaf index");
        return input - (I(1) << (order-1));
    }

    template <typename I>
    auto constexpr n546_right_child(I input) -> I
    {
        auto order = std::countr_zero(input);
        // Note: It's not valid to take the child of leaf indexes
        if (order == 0) throw std::invalid_argument("can't take right child of leaf index");
        return input + (I(1) << (order-1));
    }

    static_assert(n546_parent(1u) == 2);
    static_assert(n546_parent(2u) == 4);
    static_assert(n546_parent(3u) == 2);
    static_assert(n546_parent(4u) == 8);
    static_assert(n546_left_child(2u) == 1);
    static_assert(n546_right_child(2u) == 3);
    static_assert(n546_left_child(4u) == 2);
    static_assert(n546_right_child(4u) == 6);
    static_assert(n546_left_child(6u) == 5);
    static_assert(n546_right_child(6u) == 7);
    static_assert(n546_parent(7u) == 6);
    static_assert(n546_parent(5u) == 6);
    
}

#endif //FSSNAPSH_MATH_HPP
