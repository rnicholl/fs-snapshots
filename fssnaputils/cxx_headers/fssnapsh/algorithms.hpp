//
// Created by Ryan P. Nicholl on 7/4/22.
//

#ifndef FSSNAPSH_ALGORITHMS_HPP
#define FSSNAPSH_ALGORITHMS_HPP
#include <cinttypes>
#include <cstdint>
#include <cstddef>
#include <utility>

namespace rpnx
{
    template <typename It, typename F>
    auto foreach_up_to(It it, It end, F&& f, std::size_t n) -> std::pair<It, std::size_t>
    {
        std::size_t i = 0;
        for (; i < n && it != end; i++)
        {
            f(*it++);
        }
        return {it, i};
    }

    template <typename It, typename It2>
    auto copy_up_to(It it, It end, It2 out, std::size_t n) -> std::tuple<It, It2, std::size_t>
    {
        std::size_t i = 0;
        for (; i < n && it != end; i++)
        {
            *out++ = *it++;
        }
        return {it, out, i};
    }
}

#endif //FSSNAPSH_ALGORITHMS_HPP
