//
// Created by Ryan P. Nicholl on 7/1/22.
//

#ifndef FSSNAPSH_N546_HASH_HPP
#define FSSNAPSH_N546_HASH_HPP
#include <utility>
#include <cinttypes>
#include "rubidium.h"
#include "fssnapsh/algorithms.hpp"
#include <utility>
#include <array>
namespace rpnx
{
    template <std::size_t N>
    class blake2b
    {
    public:
        using hash_type = std::array<std::byte, N/8>;
        class incremental_hasher
        {
            rubidium_generichash_blake2b_state m_state;
        public:
            incremental_hasher() {
                ::rubidium_generichash_blake2b_init(&m_state, nullptr, 0, N/8);
            }

            incremental_hasher(blake2b<N>)
             : incremental_hasher() {}

             void clear()
             {
                ::rubidium_generichash_blake2b_init(&m_state, nullptr, 0, N/8);
             }

            template <typename It>
            void update(It it, It end)
            {
                std::array<std::byte, 128> buffer{};
                while (it != end)
                {
                    std::size_t count;
                    std::tie(it, std::ignore, count) = rpnx::copy_up_to(it, end, buffer.begin(), buffer.size() );
                    ::rubidium_generichash_blake2b_update(&m_state, (unsigned char const *) buffer.data(), count);
                }
            }

            hash_type get()
            {
                rubidium_generichash_blake2b_state state_copy = m_state;
                hash_type h;
                ::rubidium_generichash_blake2b_final(&state_copy, (unsigned char *) h.data(), h.size());
                return h;
            }
        };
    };
    template <typename Hash, std::size_t Sz, std::size_t N>
    constexpr typename Hash::hash_type n546_zhash()
    {
        std::array<std::byte, Sz> blank_segment{};
        if constexpr(N == 0)
        {
            typename Hash::incremental_hasher h;
            h.update(blank_segment.begin(), blank_segment.end());
            return h.get();
        }
        else
        {
            typename Hash::hash_type result = n546_zhash<Hash, Sz, N-1>();
            typename Hash::incremental_hasher h;
            h.update(result.begin(), result.end());
            h.update(result.begin(), result.end());
            return h.get();
        }
    }


    template <typename Hash, std::size_t segment_size, typename It, typename It2>
    void constexpr write_n546_array(It begin, It end, Hash h, It2 output)
    {

    }
}

#endif //FSSNAPSH_N546_HASH_HPP
