//
// Created by Ryan P. Nicholl on 6/25/22.
//

#ifndef FSSNAPSH_FSSNAPSH_DB_HPP
#define FSSNAPSH_FSSNAPSH_DB_HPP

#include <memory>
#include <filesystem>
#include <vector>
#include <utility>
namespace rnicholl::fssnapsh
{
    class fssnapsh_sqlite3_backend_impl;



    class fssnapsh_sqlite3_backend
    {
        std::unique_ptr<fssnapsh_sqlite3_backend_impl> m_ptr;
    public:
        [[maybe_unused]] fssnapsh_sqlite3_backend(std::filesystem::path const &path);
        ~fssnapsh_sqlite3_backend();

        void create_arena(std::string name);

        std::vector<std::byte> keystore_load(std::string key);
        void keystore_save(std::string key, std::vector<std::byte> data);
    };
}

#endif //FSSNAPSH_FSSNAPSH_DB_HPP
