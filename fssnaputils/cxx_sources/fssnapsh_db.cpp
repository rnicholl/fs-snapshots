//
// Created by Ryan P. Nicholl on 6/25/22.
//
#include "fssnapsh/fssnapsh_db.hpp"
#include "fssnapsh/fssnapsh_db_impl.hpp"

rnicholl::fssnapsh::fssnapsh_sqlite3_backend::fssnapsh_sqlite3_backend(std::filesystem::path const &path)
: m_ptr(std::make_unique<fssnapsh_sqlite3_backend_impl>(path))
{

}

rnicholl::fssnapsh::fssnapsh_sqlite3_backend::~fssnapsh_sqlite3_backend()
{

}

void rnicholl::fssnapsh::fssnapsh_sqlite3_backend::create_arena(std::string name)
{
    m_ptr->create_arena(name);
}

