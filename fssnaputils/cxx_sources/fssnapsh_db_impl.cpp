//
// Created by Ryan P. Nicholl on 6/25/22.
//
#include "fssnapsh/fssnapsh_db_impl.hpp"
#include "rpnx/sql/sqlite3pp.hpp"

rnicholl::fssnapsh::fssnapsh_sqlite3_backend_impl::fssnapsh_sqlite3_backend_impl(const std::filesystem::path &path)
: m_db(path),
  m_begin_transaction(m_db, "BEGIN TRANSACTION;"),
   m_end_transaction(m_db, "END TRANSACTION;")
{

    setup_tables();
}

rnicholl::fssnapsh::fssnapsh_sqlite3_backend_impl::~fssnapsh_sqlite3_backend_impl()
{
    destroy_setup_tables();
}

void rnicholl::fssnapsh::fssnapsh_sqlite3_backend_impl::create_arena(const std::string &name)
{
   transaction_scope scope(*this);
   m_db.exec("INSERT INTO arenas(name) VALUES (?1)", name);


}

void rnicholl::fssnapsh::fssnapsh_sqlite3_backend_impl::setup_tables()
{
    /* language=SQLite */
    m_db.exec("CREATE TABLE IF NOT EXISTS arenas(name TEXT UNIQUE, id INTEGER PRIMARY KEY);");
    /* language=SQLite */
    m_db.exec("CREATE TABLE IF NOT EXISTS current_snapshots(name TEXT UNIQUE PRIMARY KEY, hash TEXT);");
    // Unecrypted blobs stores a reference counted list of blobs as well as blob data.
    /* language=SQLite */
    m_db.exec("CREATE TABLE IF NOT EXISTS directories(arena TEXT NOT NULL, id INTEGER PRIMARY KEY, usages INTEGER);");
    /* language=SQLite */
    /* language=SQLite */
    m_db.exec("create table if not exists file_entries(arena TEXT NOT NULL, dir TEXT NOT NULL, entry_name TEXT NOT NULL, is_file INT NOT NULL, id INTEGER NOT NULL)");
    /* The directory_entries table contains each directory and how many files are in it.
     */
    // language=SQLite
    m_db.exec("create table if not exists directory_entries(arena TEXT NOT NULL, dir INTEGER NOT NULL, entry_name TEXT NOT NULL, is_file INT NOT NULL, id INTEGER NOT NULL, UNIQUE(dir, entry_name) )");

    /* language=SQLite */
    m_db.exec("CREATE TABLE IF NOT EXISTS unecnrypted_blobs(hash TEXT UNIQUE NOT NULL, hash_alg TEXT UNIQUE NOT NULL, data BLOB, datapath TEXT, usage_count INTEGER NOT NULL);");
    // The unencrypted cache is used to decide when to clear data from the blobs.
    // It doesn't contain any data by itself, but removing an entry from the cache is used to
    // decrease the usage count in the unencrypted_blobs table.
    /*language=SQLite */
    m_db.exec("CREATE TABLE IF NOT EXISTS unecnrypted_cache (hash TEXT NOT NULL, age INTEGER);");


}

void rnicholl::fssnapsh::fssnapsh_sqlite3_backend_impl::destroy_setup_tables()
{
    m_setup_tables.clear();

}

void rnicholl::fssnapsh::fssnapsh_sqlite3_backend_impl::commit_transaction()
{

}


rpnx::sqlite3pp::statement rnicholl::fssnapsh::fssnapsh_sqlite3_backend_impl::prepare_statement(const char *statment)
{
    return rpnx::sqlite3pp::statement(m_db, statment);
}



void rnicholl::fssnapsh::fssnapsh_sqlite3_backend_impl::begin_transaction()
{
    m_begin_transaction.reset();
    auto result = m_begin_transaction.step();
    if (result != SQLITE_DONE )
    {
        throw std::runtime_error("Failed to execute step");
    }

}

void rnicholl::fssnapsh::fssnapsh_sqlite3_backend_impl::end_transaction()
{
    m_end_transaction.reset();
    auto result = m_end_transaction.step();
    if (result != SQLITE_DONE )
    {
        throw std::runtime_error("Failed to execute step");
    }

}

void rnicholl::fssnapsh::fssnapsh_sqlite3_backend_impl::abort_transaction()
{
    m_db.exec("ROLLBACK;");
}

void rnicholl::fssnapsh::fssnapsh_sqlite3_backend_impl::add_arena_dirent(std::string const & hash)
{
    transaction_scope scope(*this);
    m_db.exec("INSERT OR IGNORE INTO dir_entities(hash, usages) VALUES(?1, 0);", hash);
    m_db.exec("update dir_entities set usages = usages + 1 where hash = ?1", hash);
}

void rnicholl::fssnapsh::fssnapsh_sqlite3_backend_impl::snapshot_push_file_hash(std::string arena, std::string path,
                                                                                std::string hash, std::size_t snapshot_id)
{
    transaction_scope scope(*this);
    std::vector<std::tuple<std::string, std::string> > reuslts;
    m_db.query(std::back_inserter(reuslts), "SELECT hash FROM file_hashes WHERE arena = ?1 AND path = ?2", arena, path);
    m_db.exec("INSERT OR REPLACE INTO file_hashes(arena, path, hash) VALUES(?1, ?2, ?3)",arena, path, hash);

}
