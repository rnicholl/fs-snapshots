//
// Created by Ryan P. Nicholl on 6/25/22.
//

#ifndef FSSNAPSH_FSSNAPSH_DB_IMPL_HPP
#define FSSNAPSH_FSSNAPSH_DB_IMPL_HPP

#ifndef FSSNAPSH_PRIVATE
#error This implementation header is not meant to be included by external consumers of the library.
#endif

#include <sqlite3.h>
#include <filesystem>
#include <vector>
#include <optional>

#include "rpnx/sql/sqlite3pp.hpp"
#include "rpnx/sql/database.hpp"


namespace rnicholl::fssnapsh
{
    class fssnapsh_sqlite3_backend_impl
    {
        rpnx::sqlite3pp::database m_db;
        std::vector<rpnx::sqlite3pp::statement> m_setup_tables;
        rpnx::sqlite3pp::statement m_begin_transaction;
        rpnx::sqlite3pp::statement m_end_transaction;
        std::size_t m_transaction_levels;

        void commit_transaction();

        void begin_transaction();
        void end_transaction();

        void abort_transaction();

        class transaction_scope
        {
            std::size_t num_except;
            fssnapsh_sqlite3_backend_impl & db;

        public:
            transaction_scope(fssnapsh_sqlite3_backend_impl & parent)
            : db(parent)
            {
                db.m_transaction_levels++;
                num_except = std::uncaught_exceptions();
                if (db.m_transaction_levels == 1)
                {
                    db.begin_transaction();
                }
            }
            ~transaction_scope() noexcept(false)
            {
                --db.m_transaction_levels;
                if (db.m_transaction_levels == 0 && std::uncaught_exceptions() == num_except)
                {
                    db.commit_transaction();
                }
                else if (db.m_transaction_levels == 0)
                {
                    db.abort_transaction();
                }
            }
        };
    public:
        explicit fssnapsh_sqlite3_backend_impl(std::filesystem::path const &path);
        ~fssnapsh_sqlite3_backend_impl();


        void create_arena(std::string const &);
        //void update_arena_path(std::string const & arena, std::string path);
        void add_arena_dirent(std::string const & hash);

        void add_hash_content(std::string hash, std::vector<std::byte> content);


        // Creates a vault
        // Vaults can store information
        // You can use different keys to store and withdraw information from a vault.

        /** Creates a new vault handle.
         *
         * @return vault ID
         */
        std::size_t vault_init();

        /**
         * Adds a password to withdraw messages from a vault.
         * @param vault_id The vault to use.
         * @param password Password string.
         */
        void vault_add_withdraw_password(std::size_t vault_id, std::string password);

        /**
         * Adds a private key to be used to withdraw messages from the vault.
         * @param vault_id Which vault to use.
         * @param key The Curve25519 private key to use.
         */
        void vault_add_withdraw_key_curve25519(std::size_t vault_id, std::string key);

        /**
         * Adds a private key that can be used to deposit messages into the vault.
         * @param vault_id
         * @param key
         */
        void vault_add_deposit_key(std::size_t vault_id, std::string key);
        void vault_add_deposit_password(std::size_t vault_id, std::string password);
        void vault_deposit_allow_insecure(std::size_t vault_id);

        void vault_lock(std::size_t);
        void vault_unlock(std::size_t);

        void vault_deposit_message(std::size_t vault_id);

        // Step 1. Start snapshot.
        // Step 2.

        std::size_t snapshot_create(std::string arena);
        void snapshot_set_machinename(std::size_t snapshot, std::string name);
        void snapshot_set_password(std::string password);
        void snapshot_set_pubkey(std::string public_key);
        void snapshot_push_file_hash(std::string arena, std::string path, std::string hash, std::size_t snapshot_id);
        void snapshot_push_directory_hash(std::string arena, std::size_t snapshot_id, std::string path, std::string hash);
        //void push_file_hash_into_snapshot(std::string arena, std::size_t snapshot);
        std::vector<std::string> snapshot_get_missing_blobs(std::size_t nblobs);
        void snapshot_push_blob(std::string hash, std::vector<std::byte>);
        void save_snapshot(std::size_t);





    private:
        void setup_tables();
        void destroy_setup_tables();


        rpnx::sqlite3pp::statement prepare_statement(const char *statment);


    };
}

#endif //FSSNAPSH_FSSNAPSH_DB_IMPL_HPP
