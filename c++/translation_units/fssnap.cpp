//
// Created by rnicholl on 1/14/22.
//


#include <filesystem>
#include <string>
#include <tuple>
#include <fstream>
#include <array>
#include <assert.h>
#include "rubidium.h"
#include "fssnapsh/fssnapsh_db.hpp"
#include "../../sqlite3pp/cxx_headers/rpnx/sql/database.hpp"
#include <string_view>
#include <iostream>
#include <utility>
#include <tuple>
#include <rpnx/serial3.hpp>
#include "fssnapsh/n546_hash.hpp"
//#include <cuchar> // macos bug: header does not exist
#include <cwchar>

static std::string from_u8(std::u8string_view input) {
    std::string output;
    std::array < char, MB_LEN_MAX > data = {};
    std::mbstate_t state = {};
    std::size_t n = 0;
    while (n != input.size())
    {

    }
    return output;
}

using blake2b_hash_type = std::array<std::byte, 32>;
static_assert(rubidium_generichash_blake2b_BYTES == 32);

template <typename It>
blake2b_hash_type blake2b_hash(It begin, It end)
{
    blake2b_hash_type hash{};
    std::vector<std::byte> input (begin, end);
    rubidium_generichash_blake2b((unsigned char*)hash.data(), hash.size(), (unsigned char *) input.data(), input.size(), nullptr, 0);
    return hash;
}

std::string blake2b_hash_to_string(blake2b_hash_type hash)
{
    std::string output;
    for (std::byte by : hash)
    {
        int a = std::to_integer<unsigned int>(by) & 0x0f;
        assert(a >= 0 && a < 16);
        int b = std::to_integer<unsigned int>(by) >> 4;
        assert(b >= 0 && b < 16);
        if (b >= 10) output += 'A' + (b-10);
        else output += '0' + b;
        if (a >= 10) output += 'A' + (a-10);
        else output += '0' + a;
    }
    return output;
}

blake2b_hash_type blake2b_string_to_hash(std::string_view input)
{
    blake2b_hash_type output{};

    assert(input.size() == output.size() *2);

    for (int i = 0; i < output.size() * 2; i ++)
    {
        int c = 0;
        int a = input.at(i);
        if (a >= 'A' && a <= 'Z')
        {
            c = a - 'A' + 10;
        }
        else if (a >= 'a' && a <= 'z')
        {
            c = a - 'a' + 10;
        }
        else if (a >= '0' && a <= '9')
        {
            c = a - '0';
        }
        else
        {
            throw std::runtime_error("unexpected input data");
        }
        assert(c >= 0 && c < 16);
        if (i & 1)
        {
            std::byte initial = output[i/2];
            output[i/2] = std::byte(std::to_integer<unsigned char>(initial) | (c << 4));
        }
        else
        {
            output[i/2] = std::byte(c);
        }

    }

    return output;
}

class ArtifactStore
{
private:
    std::filesystem::path m_path;
public:
    ArtifactStore(std::filesystem::path path)
    : m_path(path)
    {

    }

    template <typename Out>
    void keystore_load_outit(std::string name, Out out)
    {
        std::ifstream f(m_path / name, std::ios::binary | std::ios::ate);
        std::streamsize sz = f.tellg();
        f.seekg(0, std::ios::beg);
        std::vector<std::byte> data(sz);
        if (!f.read((char*)data.data(), sz))
        {
            throw std::runtime_error("failed to read file");;
        }
        for (std::byte c : data) *out++ = c;
    }

    template <typename Out>
    void keystore_load_data(std::string name, Out &out)
    {
        std::filesystem::path p = m_path;

        std::ifstream f(m_path / name, std::ios::binary | std::ios::ate);
        std::streamsize sz = f.tellg();
        f.seekg(0, std::ios::beg);
        out.resize(sz);
        if (!f.read((char*)out.data(), sz))
        {
            throw std::runtime_error("failed to read file");;
        }
    }

    template <typename In>
    void keystore_save_data(std::string name, In in)
    {
        std::ofstream f(m_path / name, std::ios::binary | std::ios::trunc);
        if (!f.write((char*)in.data(), in.size()))
        {
            throw std::runtime_error("failed to write file");;
        }
    }
};

std::vector<std::byte> load_content(blake2b_hash_type content_hash, ArtifactStore& datastore)
{

    std::vector<std::byte> data;
    std::string name = "blake2b/" + blake2b_hash_to_string(content_hash);
    datastore.keystore_load_data(name, data);
    auto actual_hash = blake2b_hash(data.begin(), data.end());
    if (content_hash != actual_hash)
    {
        throw std::runtime_error("Datastore data corrupted");
    }
    return data;
}

blake2b_hash_type save_content(std::vector<std::byte> const & data, ArtifactStore &datastore)
{
    blake2b_hash_type content_hash;
    content_hash = blake2b_hash(data.begin(), data.end());
    std::string name = "blake2b/" + blake2b_hash_to_string(content_hash);

    datastore.keystore_save_data(name, data);
    auto loading_test = load_content(content_hash, datastore);
    return content_hash;
}


struct file_segment_description
{
    std::size_t m_block_pos;
    std::size_t m_file_pos;
    std::size_t m_size;

    blake2b_hash_type m_content_hash;

    auto tie()
    {
        return std::tie(m_block_pos, m_file_pos, m_size, m_content_hash);
    }
    auto tie() const
    {
        return std::tie(m_block_pos, m_file_pos, m_size, m_content_hash);
    }


    bool operator ==(file_segment_description const&) const = default;

    bool operator !=(file_segment_description const&) const = default;


};

template <>
struct rpnx::serial3::default_format<file_segment_description>
{
    using type = rpnx::serial3::format_sequence<
            rpnx::serial3::format_uintany,
            rpnx::serial3::format_uintany,
            rpnx::serial3::format_uintany,
            rpnx::serial3::format_array<std::byte, 32>
                    >;
};

template <>
struct rpnx::serial3::input_binding<file_segment_description>
{
    file_segment_description const * m_in;
public:
    void initialize(file_segment_description const & fi ) { m_in = & fi; }
    void finalize(){}
    auto use() { return m_in->tie(); }
};

template <>
struct rpnx::serial3::output_binding<file_segment_description>
{
    file_segment_description  * m_in;
public:
    void initialize(file_segment_description  & fi ) { m_in = & fi; }
    void finalize(){}
    auto use() { return m_in->tie(); }
};

struct file_info
        : std::tuple<std::vector<file_segment_description>>
{
     auto && content_segments () { return std::get<0>(*this); };
     auto && content_segments () const { return std::get<0>(*this); };

};

static_assert(!rpnx::serial3::setlike<std::map<std::u8string, file_info>>);

template <>
struct rpnx::serial3::input_binding<file_info>
{
    file_info const * m_in;
public:
    void initialize(file_info const & fi ) { m_in = & fi; }
    void finalize(){}
    auto&& use() { return m_in->content_segments(); }
};
template <>
struct rpnx::serial3::output_binding<file_info>
{
    file_info * m_in;
public:
    void initialize(file_info & fi ) { m_in = & fi; }
    void finalize(){}
    auto&& use() { return m_in->content_segments(); }
};



struct dir_snapshot
{
    std::map<std::u8string, file_info> m_all_files;

    auto operator ==(dir_snapshot const&other) const {
        return m_all_files == other.m_all_files;
    };
    auto operator !=(dir_snapshot const&other) const {
        return m_all_files != other.m_all_files;
    };
};

template <>
struct rpnx::serial3::input_binding<dir_snapshot>
{
    dir_snapshot const * m_in;
public:
    void initialize(dir_snapshot const & fi ) { m_in = & fi; }
    void finalize(){}
    auto&& use() { return m_in->m_all_files; }
};

template <>
struct rpnx::serial3::output_binding<dir_snapshot>
{
    dir_snapshot  * m_in;
public:
    void initialize(dir_snapshot & fi ) { m_in = & fi; }
    void finalize(){}
    auto&& use() { return m_in->m_all_files; }
};




std::vector<std::byte> load_file_data(std::filesystem::path const & path)
{
    std::vector<std::byte> out;
    std::ifstream f(path, std::ios::binary | std::ios::ate);
    std::streamsize sz = f.tellg();
    f.seekg(0, std::ios::beg);
    out.resize(sz);
    if (!f.read((char*)out.data(), sz))
    {
        throw std::runtime_error("failed to read file");;
    }
    return out;
}

std::vector<file_segment_description> store_data_as_segments(ArtifactStore& storage, std::vector<std::byte> const & data)
{
    std::vector<std::byte> buffer;
    buffer.resize(4096*32);

    std::vector<file_segment_description> segments;

    for(std::size_t i = 0; i < data.size(); i+= buffer.size())
    {
        if (i + buffer.size() > data.size())
        {
            buffer.resize(data.size()- i);
        }
        file_segment_description segment;
        buffer.assign(data.begin()+i, data.begin()+i+buffer.size());
        segment.m_content_hash = save_content(buffer, storage);
        segment.m_file_pos = i;
        segment.m_block_pos = 0;
        segment.m_size = buffer.size();
        segments.push_back(segment);

    }

    return segments;

}


dir_snapshot create_dir_snapshot(ArtifactStore & storage, std::filesystem::path dir)
{
    dir_snapshot snapshot;
    for (auto & x: std::filesystem::recursive_directory_iterator(dir))
    {
        //std::cerr << x.path() << std::endl;
        if (x.path().filename() == ".DS_Store") continue;
        if (x.is_regular_file())
        {
            auto data = load_file_data(x.path());
            file_info info_for_file;
            info_for_file.content_segments() = store_data_as_segments(storage, data);
            auto path = std::filesystem::relative(x.path(), dir);
            snapshot.m_all_files[path.u8string()]  = info_for_file;
        }
    }
    return snapshot;
}

void write_file_from_segments(ArtifactStore &storage, std::vector<file_segment_description> const & data, std::filesystem::path p)
{
    if (std::filesystem::exists(p)) throw std::runtime_error("file already exists");
    std::ofstream file(p, std::ios::binary | std::ios::trunc);

    if (!file) throw std::runtime_error("Failed to open file for writing");

    for (auto & chunk : data)
    {
        file.seekp(chunk.m_file_pos);
        auto data_chunk = load_content(chunk.m_content_hash, storage);
        data_chunk.erase(data_chunk.begin(), data_chunk.begin() + chunk.m_block_pos);
        if (data_chunk.size() < chunk.m_size)
        {
            throw std::runtime_error("wut");
        }

        data_chunk.resize(chunk.m_size);

        if (! file.write((char*)data_chunk.data(), data_chunk.size())) {
            throw std::runtime_error("Failed to write file data");
        }
    }
}


std::vector<std::byte> snapshot_struct_to_bin(dir_snapshot const & ss)
{
    std::vector<std::uint8_t> output;
    rpnx::serial3::iterator_serialize(ss,std::back_inserter(output));
    std::vector<std::byte> output_bytes;
    for (auto x : output) output_bytes.push_back(std::byte(x));
    return output_bytes;
}

void restore_from_snapshot(ArtifactStore & storage, dir_snapshot ss, std::filesystem::path location)
{
    for (auto f : ss.m_all_files)
    {
        auto filepath = location / f.first;
        std::cerr << filepath << std::endl;
        write_file_from_segments(storage, f.second.content_segments(), filepath);
    }
}

int main(int argc, char ** argv)
{
    std::string message = "hello";
    std::vector<std::byte> data;
    for (char c : message) data.push_back(std::byte(c));

    auto hash = rpnx::n546_zhash<rpnx::blake2b<256>, 1024, 0>();
    auto hash2 = rpnx::n546_zhash<rpnx::blake2b<256>, 1024, 1>();
    std::array<std::byte, 1> ary{std::byte('f')};
    std::string f = "f";
    std::cout << blake2b_hash_to_string(blake2b_hash(ary.begin(), ary.end())) << std::endl;
    std::cout << blake2b_hash_to_string(rpnx::n546_zhash<rpnx::blake2b<256>, 1024, 0>()) << std::endl;
     std::cout << blake2b_hash_to_string(rpnx::n546_zhash<rpnx::blake2b<256>, 1024, 1>()) << std::endl;

    return EXIT_SUCCESS;

    ArtifactStore store(std::string(getenv("HOME")) + "/test");


    rnicholl::fssnapsh::fssnapsh_sqlite3_backend backend(std::string(getenv("HOME")) + "/fssnapsh.db");
    rpnx::sqlite3pp::database db(std::string(getenv("HOME"))+ "/testingdb.db");


    db.exec("CREATE TABLE IF NOT EXISTS foo(name TEXT, time INTEGER);");
    db.exec("INSERT OR REPLACE INTO foo(name, time) VALUES (?1, ?2);", "hello", 9);
    db.exec("INSERT OR REPLACE INTO foo(name, time) VALUES (?1, ?2);", "world", 7);
    db.exec("INSERT INTO foo(name, time) VALUES (?1, ?2);", "world", 8);

    std::vector< std::tuple<std::string, std::int64_t> > results;
    db.query(std::back_inserter(results), "SELECT name, time FROM foo WHERE name = ?1", "world" );

    backend.create_arena("testing_arena");

    std::cout << "results: " << std::endl;
    for (auto & x : results)
    {
        std::cout << "  -  " << std::get<0> (x) << " -- " << std::get<1>(x) << std::endl;
    }

    save_content(data, store);
    auto snapshot = create_dir_snapshot(store, std::string(getenv("HOME"))  + "/backup/");

    for (auto & f :snapshot.m_all_files)
    {
        std::cerr << (char*) f.first.data() << std::endl;
        std::cerr << f.second.content_segments().size() << std::endl;
    }

    std::ofstream snapshot_desc_to_save(std::string(getenv("HOME")) + "/snapshot.bin",std::ios::binary | std::ios::trunc );
    rpnx::serial3::iterator_serialize(snapshot, std::ostreambuf_iterator<char>(snapshot_desc_to_save));
    snapshot_desc_to_save.close();
    std::ifstream snapshot_loader(std::string(getenv("HOME")) + "/snapshot.bin", std::ios::binary);

    dir_snapshot ld_snapshot;
    rpnx::serial3::iterator_deserialize(ld_snapshot, std::istreambuf_iterator<char>(snapshot_loader), std::istreambuf_iterator<char>());

    std::cout << "worked? " << (snapshot == ld_snapshot) << std::endl;
    //restore_from_snapshot(store, snapshot, std::string(getenv("HOME"))  + "/restore");

}

